" General ----------------------------------------------------------------- {{{

silent function! NEOVIM()
    return has('nvim')
endfunction

silent function! RUBY()
    return has('ruby')
endfunction

silent function! CMDT()
    return NEOVIM() && RUBY()
endfunction

" leader
let mapleader = ","
let maplocalleader = "\\"

" Plugins {{{

call plug#begin('~/.vim/bundle')

Plug 'itchyny/lightline.vim'
" {{{
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }
" }}}
Plug 'jlanzarotta/bufexplorer'
" {{{
let g:bufExplorerDefaultHelp=0
let g:bufExplorerFindActive=1
let g:bufExplorerShowRelativePath=1
let g:bufExplorerSortBy='name'

" explore the buffers with ,o
map <leader>o :BufExplorer<cr>
" }}}
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" {{{
" set up the ripgrep command
let g:rg_command = '
  \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
  \ -g "*.{js,json,php,md,styl,jade,html,config,py,cpp,c,go,hs,rb,conf}"
  \ -g "!{.git,node_modules,vendor}/*" '

" make :F call rg_command
command! -bang -nargs=* F call fzf#vim#grep(g:rg_command .shellescape(<q-args>), 1, <bang>0)

" map `F` to rg_command
nnoremap F :F<space>
" }}}
Plug 'junegunn/goyo.vim'
" {{{
nnoremap <leader>f :Goyo<cr>
let g:goyo_width = 110
let g:goyo_height = 80
" }}}
if v:version >= 800
    Plug 'ludovicchabant/vim-gutentags'
endif
" {{{
let g:gutentags_ctags_exclude = ['*.css', '*.html', '*.js']
let g:gutentags_cache_dir = '~/.vim/gutentags'

" ,a jump to definition. ,t jump back
nnoremap <leader>a <C-]>
nnoremap <leader>t <C-t>
nnoremap <leader>A :tselect<CR>
" }}}
Plug 'scrooloose/nerdtree'
" {{{
noremap <F2> :NERDTreeToggle<cr>
inoremap <F2> <esc>:NERDTreeToggle<cr>

augroup ps_nerdtree
    au!
    au Filetype nerdtree setlocal nolist
    au Filetype nerdtree nnoremap <buffer> H :vertical resize -10<cr>
    au Filetype nerdtree nnoremap <buffer> L :vertical resize +10<cr>
augroup END

let NERDTreeHighlightCursorline = 1
let NERDTreeIgnore = ['\~$', '.*\.pyc$', 'pip-log\.txt$', 'whoosh_index',
                    \ 'xapian_index', '.*.pid', '.*-fixtures-.*.json',
                    \ '.*\.o$', 'db.db', 'tags.bak', '.*\.pdf$', '.*\.mid$',
                    \ '^tags$',
                    \ '^.*\.meta$',
                    \ '.*\.bcf$', '.*\.blg$', '.*\.fdb_latexmk$', '.*\.bbl$', '.*\.aux$', '.*\.run.xml$', '.*\.fls$',
                    \ '.*\.midi$']
let g:NERDTreeWinPos = 'right'
let NERDChristmasTree = 1
let NERDTreeChDirMode = 2
let NERDTreeDirArrows = 1
let NERDTreeMapJumpFirstChild = 'gK'
let NERDTreeMinimalUI = 1
" }}}
Plug 'Shougo/deoplete.nvim', NEOVIM() ? {} : { 'on': [] }
" {{{
let g:deoplete#enable_at_startup = 1

inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" }}}
Plug 'tpope/vim-fugitive'
" {{{
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gd :Gdiff<cr>
" }}}
Plug 'w0rp/ale'
if CMDT()
    Plug 'wincent/command-t', {'do':':UpdateRemotePlugins'}
" {{{
nnoremap <leader>, :CommandT<cr>
nnoremap <leader>. :CommandTBuffer<cr>

let g:CommandTMaxFiles = 20000
let g:CommandTSuppressMaxFilesWarning = 1
let g:CommandTMatchWindowAtTop = 1
let g:CommandTWildIgnore=&wildignore . ",*/tmp/*,*/node_modules/*,*/bundle/*"
" }}}
else
    Plug 'ctrlpvim/ctrlp.vim'
" {{{
nnoremap <leader>, :CtrlP<cr>
nnoremap <leader>. :CtrlPBuffer<cr>

let g:ctrlp_match_window_bottom = 0
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
" }}}
endif
if NEOVIM()
    Plug 'fatih/vim-go', {'do':':GoInstallBinaries'}
" {{{
let g:go_fmt_command = "goimports"
" }}}
endif
Plug 'vimwiki/vimwiki'
" {{{
let g:vimwiki_list = [{'path':'~/.wiki/', 'path_html':'~/.wiki/html/'}]
" }}}
Plug 'cespare/vim-toml'
Plug 'airblade/vim-gitgutter'
Plug 'majutsushi/tagbar'
Plug 'buoto/gotests-vim'

call plug#end()

" }}}
" Set options {{{

set autoindent                                   " copy indent from current line when starting a new
set autoread                                     " when a file has been changed outside of vim read it again
set autowrite                                    " write contents of file when modified and certain sequences occur e.g. :start
set backspace=indent,eol,start                   " make backspace work like most other apps
set backupskip=/tmp/*,/private/tmp/*"            " make Vim able to edit crontab files again.
set colorcolumn=+1                               " highlight column after 'textwidth'
set encoding=utf-8                               " make stuff work
set hidden                                       " buffer becomes hidden when it is abandoned
set history=1000                                 " remember 1000 lines of history
set laststatus=2                                 " always show status line
set lazyredraw                                   " screen wont be redrawn while executing macros/registers
set linebreak                                    " vim will wrap long lines at char `breakat`
set listchars=tab:>\ ,eol:¬                      " change tab/eol chars etc.
set matchtime=3                                  " tenths of a second to show the matching brackets etc.
set modelines=0                                  " sets the number of lines (at the beginning and end of each file) vim checks for initialisations
set mouse=a                                      " lets use the mouse
set mousehide                                    " hide the mousecursor while typing
set number                                       " turn on line numbers by default
set pastetoggle=<F3>                             " binding to toggle paste mode
set relativenumber                               " show relative line numbers
set ruler                                        " shows the line and column number of the current cursor position
set shiftround                                   " round indent to multiple of `shiftwidth`
set showbreak=↪                                  " string at start of lines that have been wrapped
set showcmd                                      " show partial command in last line of screen, e.g. when selecting chars shows number of chars
set showmode                                     " show whether we're in insert, visual, or replace mode etc.
set splitbelow                                   " splitting a window will put the new window below current
set splitright                                   " splitting a window will put the new window right of current
set synmaxcol=800                                " don't try to highlight lines longer than 800 characters.
set title                                        " title of the window will be set to value of `titlestring`
set ttyfast                                      " indicates a fast terminal connection
set undofile                                     " save undo's after file closes
set undoreload=10000                             " number of lines to save for undo
set visualbell                                   " uses a visual bell instead of beeping noise

" time out on key codes but not mappings
" e.g. `ci` will not timeout until you press t or ( or ESC etc.
set notimeout
set ttimeout
set ttimeoutlen=10

" better completion
set complete=.,w,b,u,t
set completeopt=longest,menuone

" save when losing focus
au FocusLost * :silent! wall

" resize splits when the window is resized
au VimResized * :wincmd =


" }}}
" Spelling {{{

" three dictionaries used for spellchecking:
"   /usr/share/dict/words
"       basic stuff
"   ~/.vim/custom-dictionary.utf-8.add
"       custom word list
" also remap zG to add to the local dict
set dictionary=/usr/share/dict/words
set spellfile=~/.vim/custom-dictionary.utf-8.add
nnoremap zG 2zg

"}}}
" Cursorline {{{

" Only show cursorline in the current window and in normal mode.
set cursorline " required for cursorline on initial open
augroup cline
    au!
    au WinLeave,InsertEnter * set nocursorline
    au WinEnter,InsertLeave * set cursorline
augroup END

" }}}
" Trailing Whitespace {{{

" only shown when not in insert mode
augroup trailing
    au!
    au InsertEnter * :set listchars-=trail:⌴
    au InsertLeave * :set listchars+=trail:⌴
augroup END

" }}}
" Wildmenu Completion {{{

set wildmenu " completion e.g. :color <TAB>
set wildmode=list:longest

set wildignore+=*.aux,*.out,*.toc                      " LaTeX intermediate files
set wildignore+=*.DS_Store                             " macOS bullshit
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg,*.ico   " binary images
set wildignore+=*.luac                                 " Lua byte code
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest       " compiled object files
set wildignore+=*.orig                                 " Merge resolution files
set wildignore+=*.pyc                                  " Python byte code
set wildignore+=*.spl                                  " compiled spelling word lists
set wildignore+=*.sw?                                  " Vim swap files
set wildignore+=.hg,.git,.svn                          " Version control
set wildignore+=migrations                             " Django migrations
set wildignore+=node_modules                           " JavaScript node_modules
set wildignore+=target                                 " dist/target
set wildignore+=dist                                   " dist/target

" }}}
" Line Return {{{

" make sure Vim returns to the same line when you reopen a file
augroup line_return
    au!
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \     execute 'normal! g`"zvzz' |
        \ endif
augroup END

" }}}
" Tabs, Spaces, Wrapping {{{

set expandtab           " insert mode, uses spaces to insert <Tab>
set formatoptions=qrn1j " how vim will auto format
set shiftwidth=4
set softtabstop=4       " number of spaces a <Tab> counts for while editing
set tabstop=4           " number of spaces a <Tab> counts for
set textwidth=80        " marker on 80 chars
set wrap                " lines longer than the width of the window will wrap

" }}}
" Backups {{{

set backup                        " enable backups
set noswapfile                    " stop creating swapfiles

set backupdir=~/.vim/tmp/backup// " backups
set directory=~/.vim/tmp/swap//   " swap files
set undodir=~/.vim/tmp/undo//     " undo files

" make above folders automatically if they don't already exist
if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
endif

" }}}
" Color Scheme {{{

syntax enable
colorscheme gruvbox
set background=dark

" highlight VCS conflict markers
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'

" }}}

" }}}
" Folding ----------------------------------------------------------------- {{{

set foldmethod=marker " fold with {{{ }}}
set foldlevelstart=0

function! MyFoldText() " {{{
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction " }}}
set foldtext=MyFoldText()

" Make zO recursively open whatever fold we're in, even if it's partially open.
nnoremap zO zczO

" space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za

" use ,z to "focus" the current fold
nnoremap <leader>z zMzvzz

" Diff Folding {{{

" This is from https://github.com/sgeb/vim-diff-fold/ without the extra
" settings crap.  Just the folding expr.

function! DiffFoldLevel()
    let l:line=getline(v:lnum)

    if l:line =~# '^\(diff\|Index\)'     " file
        return '>1'
    elseif l:line =~# '^\(@@\|\d\)'  " hunk
        return '>2'
    elseif l:line =~# '^\*\*\* \d\+,\d\+ \*\*\*\*$' " context: file1
        return '>2'
    elseif l:line =~# '^--- \d\+,\d\+ ----$'     " context: file2
        return '>2'
    else
        return '='
    endif
endfunction

augroup ft_diff
    au!

    autocmd FileType diff setlocal foldmethod=expr
    autocmd FileType diff setlocal foldexpr=DiffFoldLevel()
augroup END

" }}}

" }}}
" Abbreviations ----------------------------------------------------------- {{{

" stop abbreviations inserting an extra space
function! EatChar(pat)
    let c = nr2char(getchar(0))
    return (c =~ a:pat) ? '' : c
endfunction
function! MakeSpacelessIabbrev(from, to)
    execute "iabbrev <silent> ".a:from." ".a:to."<C-R>=EatChar('\\s')<cr>"
endfunction
function! MakeSpacelessBufferIabbrev(from, to)
    execute "iabbrev <silent> <buffer> ".a:from." ".a:to."<C-R>=EatChar('\\s')<cr>"
endfunction

" add new abbreviations with format below
call MakeSpacelessIabbrev('wt/', 'https://wilkins.tech/')
call MakeSpacelessIabbrev('w@', 'wilkins@linux.com')

" to insert an abbrev --> i`abbrev` e.g. ildis<Enter>
iab ldis ಠ_ಠ
iab lhap ಥ‿ಥ
iab lmis ಠ‿ಠ
iab lsad ಥ_ಥ

" date abbreviation
iab xdate <c-r>=strftime("%d/%m/%y %H:%M:%S")<cr>

" php abbreviations
iab erlog error_log(

" }}}
" Mappings & Useful Functions --------------------------------------------- {{{

" Mappings {{{

" no help key
inoremap <F1> <Nop>
noremap  <F1> <Nop>

" stop it, hash key.
inoremap # X<BS>#
noremap # <Nop>

" kill window
nnoremap K :q<cr>

" save
nnoremap s :w<cr>

" man file for word under cursor
nnoremap M K

" toggle line numbers
nnoremap <leader>n :setlocal number!<cr>

" toggle relative numbers
nnoremap <leader>N :setlocal relativenumber!<cr>

" sort lines
nnoremap <leader>s vip:!sort -f<cr>
vnoremap <leader>s :!sort -f<cr>

" tabs
nnoremap <leader>( :tabprev<cr>
nnoremap <leader>) :tabnext<cr>

" digraph codes in insert mode ctrl-k ctrl-k
inoremap <c-k><c-k> <esc>:help digraph-table<cr>

" wrap
nnoremap <leader>W :set wrap!<cr>

" inserting blank lines
nnoremap <cr> o<esc>

" Yank to end of line
nnoremap Y y$

" Reselect last-pasted text
nnoremap gv `[v`]

" unmap `u` in visual mode since it's so close to y, remap `gu` to `u` for when it's needed
vnoremap u <nop>
vnoremap gu u

" Rebuild Ctags (mnemonic RC -> CR -> <cr>) [ install ctags ]
nnoremap <leader><cr> :silent !myctags >/dev/null 2>&1 &<cr>:redraw!<cr>

" clear trailing whitespace
nnoremap <leader>rw mz:%s/\s\+$//<cr>:let @/=''<cr>`z

" select entire buffer
nnoremap vaa ggvGg_
nnoremap Vaa ggVG

" fix for spellcheck `zz` brings up buffer to edit word under cursor
nnoremap zz z=
nnoremap z= :echo "remapped to zz"<cr>

" <C-u> will uppercase a word in insert mode
inoremap <C-u> <esc>mzgUiw`za

" panic button (reverse all text on screen)
nnoremap <f9> mzggg?G`z

" `zh` is "zoom to head level" e.g. put text under cursor at top of screen
nnoremap zh mzzt5<c-u>`z

" diffoff
nnoremap <leader>D :diffoff!<cr>

" formatting, TextMate-style
nnoremap Q gqip
vnoremap Q gq

" reformat line
nnoremap ql gqq

" easier linewise reselection of what you just pasted
nnoremap <leader>V V`]

" indent/dedent/autoindent what you just pasted
nnoremap <lt>> V`]<
nnoremap ><lt> V`]>
nnoremap =- V`]=

" keep the cursor in place while joining lines
nnoremap J mzJ`z

" split line
" the normal use of S is covered by cc, so don't worry about shadowing it.
nnoremap S i<cr><esc>^mwgk:silent! s/\v +$//<cr>:noh<cr>`w

" substitute
nnoremap <C-s> :%s/
vnoremap <C-s> :s/

" source
vnoremap <leader>S y:@"<cr>
nnoremap <leader>S ^vg_y:execute @@<cr>:echo 'Sourced line.'<cr>

" select (charwise) the contents of the current line, excluding indentation.
nnoremap vv ^vg_

" typos e.g. WQ --> wq
command! -bang E e<bang>
command! -bang Q q<bang>
command! -bang W w<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Wq wq<bang>
command! -bang WQ wq<bang>

" Visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv

" toggle [i]nvisible characters
nnoremap <leader>i :set list!<cr>

" unfuck the screen
nnoremap U :syntax sync fromstart<cr>:redraw!<cr>

" Insert Mode Completion
inoremap <c-f> <c-x><c-f>
inoremap <c-]> <c-x><c-]>
inoremap <c-l> <c-x><c-l>

" }}}
" Functions {{{

" copying/pasting text to the system clipboard.
function! g:FuckingCopyTheTextPlease()
    let view = winsaveview()
    let old_z = @z
    normal! gv"zy
    call system('pbcopy', @z)
    let @z = old_z
    call winrestview(view)
endfunction

function! g:FuckingCopyAllTheTextPlease()
    let view = winsaveview()
    let old_z = @z
    normal! ggVG"zy
    call system('pbcopy', @z)
    let @z = old_z
    call winrestview(view)
endfunction

" ,y in normal mode copies a line - ,Y copies the whole file - ,y in visual mode copies whatever is highlighted
noremap <leader>p "*p
vnoremap <leader>y :<c-u>call g:FuckingCopyTheTextPlease()<cr>
nnoremap <leader>y VV:<c-u>call g:FuckingCopyTheTextPlease()<cr>
nnoremap <leader>Y :<c-u>call g:FuckingCopyAllTheTextPlease()<cr>

function! OpenMarkdownPreview() abort
  if exists('s:markdown_job_id') && s:markdown_job_id > 0
    call jobstop(s:markdown_job_id)
    unlet s:markdown_job_id
  endif
  let s:markdown_job_id = jobstart('grip ' . shellescape(expand('%:p')))
  if s:markdown_job_id <= 0 | return | endif
  call system('open http://localhost:6419')
endfunction

noremap <silent> <leader>om :call OpenMarkdownPreview()<cr>

" }}}

" }}}
" Quick Editing ----------------------------------------------------------- {{{

nnoremap <leader>ed :vsplit ~/.vim/custom-dictionary.utf-8.add<cr>
nnoremap <leader>eg :vsplit ~/.gitconfig<cr>
nnoremap <leader>ep :vsplit ~/.config<cr>
nnoremap <leader>et :vsplit ~/.tmux.conf<cr>
nnoremap <leader>ev :vsplit ~/.vimrc<cr>

" }}}
" Tabs & Buffers ---------------------------------------------------------- {{{

" Tabs {{{

" shift-t to open a new tab
nnoremap T :tabnew<cr>

" tl to tab-next and th to tab-prev
nnoremap tl :tabN<cr>
nnoremap th :tabp<cr>

" }}}
" Buffers {{{

" close currently open buffer
noremap <leader>bd :bd<cr>

" Switch CWD to the directory of the open buffer
noremap <leader>cd :cd %:p:h<cr>:pwd<cr>

" mappings for switching buffer via `Ngb` where N 1-99
let c = 1
while c <= 99
  execute "nnoremap <silent> " . c . "gb :" . c . "b\<cr>"
  let c += 1
endwhile

" Ctrl-N/P for next/previous buffer
noremap <C-N> :bnext<cr>
noremap <C-P> :bprev<cr>

" }}}

" }}}
" Searching & Movement ---------------------------------------------------- {{{

" use sane regexes.
nnoremap / /\v
vnoremap / /\v

set gdefault           " all matches in line substituted instead of one
set hlsearch           " highlight all the matches, can be cleared with <leader><space>
set ignorecase         " ignore case of normal letters
set incsearch          " match as you type
set showmatch          " jump to matching brackets briefly when closing tag inserted
set smartcase          " overrides ignorecase if search pattern contains upper case

set scrolloff=5        " number of lines to keep above cursor before moving screen
set sidescroll=1       " min number of columns to scroll horizontally
set sidescrolloff=10   " number of lines to keep left and right of cursor if nowrap is set

set virtualedit+=block " allow virtual editing in visual block mode

" ,<space> to clear all highlighted search matches
noremap <silent> <leader><space> :noh<cr>:call clearmatches()<cr>

" D deletes to the end of a line
nnoremap D d$

" don't move on * (search word under cursor)
nnoremap <silent> * :let stay_star_view = winsaveview()<cr>*:call winrestview(stay_star_view)<cr>

" keep search matches in the middle of the window
nnoremap n nzzzv
nnoremap N Nzzzv

" same when jumping around
nnoremap g; g;zz
nnoremap g, g,zz
nnoremap <c-o> <c-o>zz

" easier to type start and end of line bindings
noremap H ^
noremap L $
vnoremap L g_

" keep ctrl+a, ctrl+e (begin/end) of line round for legacy reasons in insert mode
inoremap <c-a> <esc>I
inoremap <c-e> <esc>A
cnoremap <c-a> <home>
cnoremap <c-e> <end>

" gi already moves to "last place you exited insert mode", so we'll map gI to move to last change
nnoremap gI `.

" fix linewise visual selection of various text objects
nnoremap VV V
nnoremap Vit vitVkoj
nnoremap Vat vatV
nnoremap Vab vabV
nnoremap VaB vaBV

" Directional Keys {{{

" wrapped lines go down/up to next row, rather than next line in file
noremap j gj
noremap k gk
noremap gj j
noremap gk k

" easy buffer navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" ,v opens a vertical split
noremap <leader>v <C-w>v

" }}}
" Visual Mode `*` behaviour {{{

function! s:VSetSearch()
  let temp = @@
  norm! gvy
  let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
  let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<cr>//<cr><c-o>
vnoremap # :<C-u>call <SID>VSetSearch()<cr>??<cr><c-o>

" }}}
" List Navigation {{{

nnoremap <left>  :cprev<cr>zvzz
nnoremap <right> :cnext<cr>zvzz
nnoremap <up>    :lprev<cr>zvzz
nnoremap <down>  :lnext<cr>zvzz

" }}}

" }}}
" Extra Configs ----------------------------------------------------------- {{{

" Highlight Word {{{
"
" this mini-plugin provides a few mappings for highlighting words temporarily via <leader>1-6 when cursor is on word

function! HiInterestingWord(n) " {{{
    " save our location.
    normal! mz

    " yank the current word into the z register.
    normal! "zyiw

    " calculate an arbitrary match ID.  Hopefully nothing else is using it.
    let mid = 86750 + a:n

    " clear existing matches, but don't worry if they don't exist.
    silent! call matchdelete(mid)

    " construct a literal pattern that has to match at boundaries.
    let pat = '\V\<' . escape(@z, '\') . '\>'

    " actually match the words.
    call matchadd("InterestingWord" . a:n, pat, 1, mid)

    " move back to our original location.
    normal! `z
endfunction " }}}

" Mappings {{{

nnoremap <silent> <leader>1 :call HiInterestingWord(1)<cr>
nnoremap <silent> <leader>2 :call HiInterestingWord(2)<cr>
nnoremap <silent> <leader>3 :call HiInterestingWord(3)<cr>
nnoremap <silent> <leader>4 :call HiInterestingWord(4)<cr>
nnoremap <silent> <leader>5 :call HiInterestingWord(5)<cr>
nnoremap <silent> <leader>6 :call HiInterestingWord(6)<cr>

" }}}
" Default Highlights {{{

" orange
hi def InterestingWord1 guifg=#000000 ctermfg=16 guibg=#ffa724 ctermbg=214
" green
hi def InterestingWord2 guifg=#000000 ctermfg=16 guibg=#aeee00 ctermbg=154
" turquiose
hi def InterestingWord3 guifg=#000000 ctermfg=16 guibg=#8cffba ctermbg=121
" light-brown
hi def InterestingWord4 guifg=#000000 ctermfg=16 guibg=#b88853 ctermbg=137
" pink
hi def InterestingWord5 guifg=#000000 ctermfg=16 guibg=#ff9eb8 ctermbg=211
" red
hi def InterestingWord6 guifg=#000000 ctermfg=16 guibg=#ff2c4b ctermbg=195

" }}}
" }}}
" Code completion && Language Specific {{{

" PHP {{{
let php_sql_query = 1
let php_htmlInStrings = 1

augroup php_error_log
    autocmd!
    autocmd FileType php nnoremap <leader>el ^vg_daerror_log('<esc>pa=' . print_r(<esc>pa, true));<cr><esc>
augroup END

" completion
autocmd FileType php setlocal omnifunc=phpcomplete#CompletePHP
set completeopt=longest,menuone
let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
" }}}
" Perl {{{
au BufNewFile *.pl s-^-#!/usr/bin/perl\r\ruse warnings;\r\r-
au FileType perl setlocal shiftwidth=2
au FileType perl setlocal tabstop=2
" }}}
" Bash {{{
au FileType sh setlocal shiftwidth=2
au FileType sh setlocal tabstop=2
" }}}
" Git {{{
" set position to the first line when editing a git commit message
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
" }}}

" }}}

" }}}
