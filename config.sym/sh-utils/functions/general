#!/bin/bash
# shellcheck source=/dev/null

# check if an executable exists e.g. command_exists git
commandexists() {
  type "$1" &> /dev/null ;
}

# `v` with no arguments opens `pwd` in vim otherwise opens location
v() {
  if [ $# -eq 0 ]; then
    $EDITOR
  else
    $EDITOR "$@"
  fi
}

# list users on the system
listusers() {
  cut -d: -f1 /etc/passwd
}

# htop current user, else htop 1st argv
utop() {
  if [ $# -eq 0 ]; then
    htop -u "$USER"
  else
    htop -u "$1"
  fi
}

# disable crontab -r
crontab() {
  # replace -r with -e
  /usr/bin/crontab "${@/-r/-e}"
}

# add current user to specified groups
joingroup() {
  for group in "$@"; do
    sudo gpasswd -a "$USER" "$group"
  done
}

# del user from specified groups
leavegroup() {
  for group in "$@"; do
    sudo gpasswd -d "$USER" "$group"
  done
}

# coloured man pages
man() {
  LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# list tmux sessions
tl() {
  tmux list-sessions
}

# create a tmux session with a name corresponding to 1st argv
tc() {
  tmux new -s "$1"
}

# attach to tmux session with name corresponding to 1st argv
ta() {
  tmux attach -t "$1"
}

# simple calculator
calc() {
  local result=""
  result="$(printf "scale=10;%s\\n" "$*" | bc --mathlib | tr -d '\\\n')"

  if [[ "$result" == *.* ]]; then
    # improve the output for decimal numbers
    # add "0" for cases like ".5"
    # add "0" for cases like "-.5"
    # remove trailing zeros
    printf "%s" "$result" |
    sed -e 's/^\./0./'  \
      -e 's/^-\./-0./' \
      -e 's/0*$//;s/\.$//'
  else
    printf "%s" "$result"
  fi
  printf "\\n"
}

# create a new directory and enter it
mkd() {
  mkdir -p "$@"
  cd "$@" || return
}

# make a temporary directory and enter it
tmpd() {
  local dir
  if [ $# -eq 0 ]; then
    dir=$(mktemp -d)
  else
    dir=$(mktemp -d -t "${1}.XXXXXXXXXX")
  fi
  cd "$dir" || return
}

# determine size of a file or total size of a directory
fs() {
  if du -b /dev/null > /dev/null 2>&1; then
    local arg=-sbh
  else
    local arg=-sh
  fi
  if [[ -n "$*" ]]; then
    du $arg -- "$@"
  else
    du $arg -- .[^.]* *
  fi
}

# shorthand for `tree` hiddenfiles and colour enabled, ignores .git, directories first, piped into less if output too big
tre() {
  tree -aC -I '.git' --dirsfirst "$@" | less -FRNX
}

# check if website is up
isup() {
  local uri=$1
  if curl -s --head  --request GET "$uri" | grep "200 OK" > /dev/null ; then
    notify-send --urgency=critical "$uri is down"
  else
    notify-send --urgency=low "$uri is up"
  fi
}

# attempt find the current operating system
findcurrentos() {
  local DISTRO="";
  local os_type="";

  os_type=$(uname);
  case "$os_type" in
    "Darwin") {
      echo "OSX"
    } ;;
  "Linux") {
    # If available, use LSB to identify distribution
    if [ -f /etc/lsb-release ] || [ -d /etc/lsb-release.d ]; then
      DISTRO=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
    else
      DISTRO=$(echo /etc/[A-Za-z]*[_-][rv]e[lr]* | grep -v "lsb" | cut -d'/' -f3 | cut -d'-' -f1 | cut -d'_' -f1)
    fi
    echo "$DISTRO" | tr '[:lower:]' '[:upper:]'
  } ;;
*) {
  echo "UNKNOWN"
} ;;
  esac;
}

# check if website is up
down4me() {
  curl -s "downforeveryoneorjustme.com/$1" | sed '/just you/!d;s/<[^>]*>//g';
}

# run $2 $1 times
runx() {
  n=$1
  shift
  while [ $(( n -= 1 )) -ge 0 ]
  do
    "$@"
  done
}

# open 1st argv in google chrome
openchrome() {
  open -a "Google Chrome" "http://$1"
}

# youtube-dl full 1080p video/audio
youtubedlhd() {
  youtube-dl "$1" -f 137+140
}

# extract audio in mp3 format
youtubedlmp3() {
  youtube-dl -v --no-mtime -o '%(stitle)s.%(ext)s' --extract-audio --audio-format mp3  "$1"
}

# open default tmux or attach, if we're in a session already then use fzf to switch between them
tm() {
  [[ -n "$TMUX" ]] && change="switch-client" || change="attach-session"
  if [ "$1" ]; then
    tmux $change -t "$1" 2>/dev/null || (tmux new-session -d -s "$1" && tmux $change -t "$1"); return
  fi
  session=$(tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux $change -t "$session" || echo "No sessions found."
}

# generic extract
extract() {
  case $1 in
    *.tar.gz) tar -xf "$1";;
    *.tgz) tar -xzf "$1";;
    *.bz2) tar jxf "$1";;
    *.tar) tar 0xvf "$1";;
    *.zip) unzip "$1";;
    *.rar) unrar e "$1";;
    *.7z) 7z x "$1";;
    *.gz) tar -xvzf "$1";;
    *) echo "Nothing to do";;
  esac
}

# connect external monitor
connectmonitor() {
  if [[ $1 == 'off' ]]; then
    xrandr --output LVDS1 --size 1366x768 && xrandr --output HDMI1 --off
  elif [[ $1 == 'on' ]]; then
    xrandr --output LVDS1 --off && xrandr --output HDMI1 --size 3440x1440
  else
    echo "Unrecognised option" && return
  fi
}

# list current jails in fail2ban
listjails() {
  sudo fail2ban-client status
}

# list status of jail in fail2ban
f2bstatus() {
  jail_name="$1"
  if [[ -n "$jail_name" ]]; then
    sudo fail2ban-client status "$jail_name"
  else
    echo -e "Missing param - Usage:\\n - f2bstatus [jail name]"
  fi
}

# unban an ip banned by fail2ban
f2bunban() {
  jail_name="$1"
  ip="$2"
  if [[ -n "$jail_name" || -n "$ip" ]]; then
    sudo fail2ban-client set "$jail_name" unbanip "$ip"
  else
    echo -e "Missing param - Usage:\\n- f2bunban [jail name] [ip]\\nUse listjails and f2bstatus to see bans and jails"
  fi
}

# watch TCP/UDP etc. connections every 2 seconds
watchconnections() {
  watch ss -stplu
}

# list running services if we have `service` available, otherwise use systemctl
listrunningservices() {
  if commandexists service; then
    sudo service --status-all | grep '+'
  else
    sudo systemctl -r --type service --all | grep " active"
  fi
}

# look at dmesg output every second
watchkern() {
  watch -n1 'dmesg | tail'
}

# show terminal colors
tcolors() {
  for i in {0..255};
    do printf "\\x1b[38;5;${i}mcolor%-5i\\x1b[0m" $i;
    if ! (( (i + 1 ) % 8 )); then
      echo;
    fi;
  done
}

# check ip
checkip() {
  curl -s http://checkip.dyndns.org/ | sed 's/[a-zA-Z<>/ :]//g'
}

# re-source the terminal
src() {
  source ~/.zshrc
}

# irc application
irc() {
  weechat
}

# basic weather
weather() {
  curl wttr.in/Edinburgh
}

# make CTRL-Z background things and unbackground them.
fg-bg() {
  if [[ $#BUFFER -eq 0 ]]; then
    fg
  else
    zle push-input
  fi
}

# paste service
pb() {
  curl -F "c=@${1:--}" "https://p.wilkins.cloud/?u=1";
}

