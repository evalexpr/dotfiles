# Init {{{

unbind-key C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# }}}

# Set options {{{

# basic options
set-option -g default-terminal "screen-256color"

set-option -s escape-time 0         # no wait time between key sequences
set-option -g base-index 1          # windows are one-indexed
set-option -g allow-rename off      # don't rename windows automatically

# left and right status
set-option -g status-left ''
set-option -g status-right "%l:%M %p ⁝ #[fg=colour208,italics]#(spotify-info-mac)"

# make tmux quiet
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

# automatically renumber window numbers on closing a pane (tmux >= 1.7)
set-option -g renumber-windows on

# }}}

# Bindings {{{

# split using | and -
unbind-key '"'
unbind-key %
bind-key | split-window -h
bind-key - split-window -v

# reload config with <C-a>r
bind-key r source-file ~/.tmux.conf

# allow alt+arrow to switch pane
bind-key -n M-Down select-pane -D
bind-key -n M-Left select-pane -L
bind-key -n M-Right select-pane -R
bind-key -n M-Up select-pane -U

# pane resizing
bind-key h resize-pane -L 5
bind-key j resize-pane -D 5
bind-key k resize-pane -U 5
bind-key l resize-pane -R 5

# <Shift>up show/hide the status bar
bind-key -n S-Up set-option -g status

# <C-a>c name new windows
bind-key C new-window
bind-key c command-prompt -p "Name:" "new-window -n '%%'"

# <Shift>right/left window switching
bind-key -n S-Left previous-window
bind-key -n S-Right next-window

# }}}

# Layout/Colors {{{

# bar colours outwith middle pane
set-option -g status-bg colour235
set-option -g status-fg colour208
set-option -g status-attr bold

# pane border (| - colors)
set-option -g pane-border-fg colour240
set-option -g pane-active-border-fg colour208

# message text (displays when prefix pressed and typing begins)
set-option -g message-bg colour235
set-option -g message-fg colour208

# pane number display <C-a>:display-panes
set-option -g display-panes-active-colour colour33
set-option -g display-panes-colour colour208

# clock <C-a>t
set-window-option -g clock-mode-colour colour208

# bell (test with sleep 5; echo -en "\a")
set-window-option -g window-status-bell-style fg=colour240,bg=colour208

# window formatting s/current/active/g
set-window-option -g window-status-format '#[bg=black]#[fg=black,bold] #I #[bg=default] #[fg=black]#W  '
set-window-option -g window-status-current-format '#[fg=black]#[bg=colour208] #I #[fg=colour208]#[bg=colour240] #W  '

# }}}

